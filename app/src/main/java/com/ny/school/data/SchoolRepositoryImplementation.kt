package com.ny.school.data


import com.ny.school.api.SchoolService
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import retrofit2.Response

class SchoolRepositoryImplementation : SchoolRepository, KoinComponent {

    private val service: SchoolService by inject()

    override suspend fun getSchoolList(): Response<List<SchoolInfo>> {
        return service.getSchoolList()
    }

    override suspend fun getSchoolSatScores(): Response<List<SchoolSatScore>> {
        return service.getSchoolSatResult()
    }

}