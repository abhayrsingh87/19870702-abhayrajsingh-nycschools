package com.ny.school.data

import com.google.gson.annotations.SerializedName

data class SchoolInfo(
    val dbn: String,
    @SerializedName("school_name") val name: String,
    val campus_name: String,
    val website: String,
    val location: String,
    val total_students: String,
    val school_email: String,
    val phone_number: String,
)
