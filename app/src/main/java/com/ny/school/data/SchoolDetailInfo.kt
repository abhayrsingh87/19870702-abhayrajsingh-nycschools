package com.ny.school.data
import java.io.Serializable

/**
 * This class will help in rendering school detail information
 */
data class SchoolDetailInfo(
    val name: String,
    val location: String,
    val website: String,
    val phone_number: String,
    val sat_critical_reading_avg_score: String,
    val sat_math_avg_score: String,
    val sat_writing_avg_score: String
) : Serializable
