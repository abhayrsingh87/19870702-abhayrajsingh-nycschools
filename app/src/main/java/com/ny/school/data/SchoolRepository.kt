package com.ny.school.data

import retrofit2.Response

interface SchoolRepository {
    suspend fun getSchoolList(): Response<List<SchoolInfo>>
    suspend fun getSchoolSatScores(): Response<List<SchoolSatScore>>
}