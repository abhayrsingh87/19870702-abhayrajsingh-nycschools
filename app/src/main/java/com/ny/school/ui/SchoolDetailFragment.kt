package com.ny.school.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.ny.school.R
import com.ny.school.data.SchoolDetailInfo
import com.ny.school.databinding.FragmentSchoolDetailBinding


/**
 * A simple [Fragment] subclass.
 * Use the [SchoolDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SchoolDetailFragment : Fragment() {

    private val selectedSchool: SchoolDetailInfo? by lazy {
        arguments?.getSerializable(
            SCHOOL_DETAIL_ARG
        ) as SchoolDetailInfo
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //setting up screen title. Should be using tool bar with no action bar theme
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.school_detail_title)
        return DataBindingUtil.inflate<FragmentSchoolDetailBinding>(
            layoutInflater,
            R.layout.fragment_school_detail,
            container,
            false
        ).apply {
            schoolDetail = selectedSchool
        }.root.also {  }
    }

    companion object {
        const val SCHOOL_DETAIL_ARG = "school_detail_arg"

        @JvmStatic
        fun newInstance() = SchoolDetailFragment()
    }
}