package com.ny.school.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.ny.school.R
import com.ny.school.viewmodel.SchoolListViewModel

class SchoolListActivity : AppCompatActivity(R.layout.activity_school_list)