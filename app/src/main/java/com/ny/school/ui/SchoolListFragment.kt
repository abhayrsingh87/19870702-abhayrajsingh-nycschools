package com.ny.school.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.ny.school.R
import com.ny.school.data.SchoolInfo
import com.ny.school.databinding.FragmentSchoolListBinding
import com.ny.school.viewmodel.SchoolListViewModel

/**
 * A simple [Fragment] subclass.
 * Use the [SchoolListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SchoolListFragment : Fragment() {

    private val schoolListViewModel: SchoolListViewModel by viewModels()

    companion object {
        fun newInstance() = SchoolListFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //setting up screen title. Should be using tool bar with no action bar theme
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.school_list_title)
        return DataBindingUtil.inflate<FragmentSchoolListBinding>(
            inflater,
            R.layout.fragment_school_list,
            container,
            false
        ).apply {
            viewModel = schoolListViewModel
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        schoolListViewModel.selectedSchoolEvent.observe(viewLifecycleOwner, Observer { event ->
            event.getContentIfNotHandled()?.let { schoolDetail ->
                //Navigating to school detail fragment
                findNavController().navigate(
                    R.id.action_schoolListFragment_to_schoolDetailFragment,
                    Bundle().also {
                        it.putSerializable(
                            SchoolDetailFragment.SCHOOL_DETAIL_ARG,
                            schoolDetail
                        )
                    }
                )
            }
        })
    }

    fun interface OnSchoolSelectedListener {
        fun onSchoolSelected(schoolInfo: SchoolInfo)
    }

}