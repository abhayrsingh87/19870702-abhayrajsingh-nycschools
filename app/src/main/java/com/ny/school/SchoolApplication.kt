package com.ny.school

import android.app.Application
import com.ny.school.api.ServiceModule
import com.ny.school.data.SchoolRepository
import com.ny.school.data.SchoolRepositoryImplementation
import org.koin.core.context.startKoin
import org.koin.dsl.module

class SchoolApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        val appModule = module {
            single<SchoolRepository> { SchoolRepositoryImplementation() }
        }

        startKoin {
            modules(listOf(appModule, ServiceModule.apiModule))
        }
    }
}