package com.ny.school.api

import retrofit2.http.GET

import com.ny.school.data.SchoolInfo
import com.ny.school.data.SchoolSatScore
import retrofit2.Response


interface SchoolService {

    @GET("s3k6-pzi2")
    suspend fun getSchoolList(): Response<List<SchoolInfo>>

    @GET("f9bf-2cp4")
    suspend fun getSchoolSatResult(): Response<List<SchoolSatScore>>

}