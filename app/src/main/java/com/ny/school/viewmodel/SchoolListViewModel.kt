package com.ny.school.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ny.school.adapter.SchoolListAdapter
import com.ny.school.data.SchoolRepository
import com.ny.school.data.SchoolInfo
import com.ny.school.data.SchoolDetailInfo
import com.ny.school.data.SchoolSatScore
import com.ny.school.util.Event
import kotlinx.coroutines.*
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import retrofit2.Response


class SchoolListViewModel : ViewModel(), KoinComponent {

    private val schoolRepositoryImplementation: SchoolRepository by inject()
    private val schoolSatScoreList = MutableLiveData<List<SchoolSatScore>>()
    private val schoolList = MutableLiveData<List<SchoolInfo>>()

    var selectedSchoolEvent = MutableLiveData<Event<SchoolDetailInfo>>()

    val adapter = ObservableField<SchoolListAdapter>()
    val showSpinner = ObservableBoolean()


    init {
        getSchoolData()
    }

    private fun getSchoolData() {
        viewModelScope.launch {
            showSpinner.set(true)
            //Calling both API together
            coroutineScope {
                val deferredSchoolList =  async { schoolRepositoryImplementation.getSchoolList() }
                val deferredSchoolSatScores= async { schoolRepositoryImplementation.getSchoolSatScores() }
                val schoolListResult = deferredSchoolList.await()
                val satScoresResult = deferredSchoolSatScores.await()
                handleSchoolListResult(schoolListResult)
                handleSatResults(satScoresResult)
                showSpinner.set(false)
            }
        }
    }

    private fun handleSatResults(satScoresResult: Response<List<SchoolSatScore>>) {
        if (satScoresResult.isSuccessful) {
            satScoresResult.body()?.apply {
                schoolSatScoreList.value = this
            }
        } else {
            //TODO: handle error
        }
    }

    private fun handleSchoolListResult(schoolInfoListResult: Response<List<SchoolInfo>>) {
        if (schoolInfoListResult.isSuccessful) {
            schoolInfoListResult.body()?.apply {
                schoolList.value = this
                //Initializing adapter for recycler view after getting list of school result
                adapter.set(SchoolListAdapter(this) {
                    val matchingSatScore =
                        schoolSatScoreList.value?.find { schoolSatScore -> schoolSatScore.dbn == it.dbn }
                    //Updating selected school information
                    selectedSchoolEvent.value = Event(getSchoolDetailInfo(it, matchingSatScore))
                })
            }
        } else {
            //TODO: handle error
        }
    }

    /**
     * Create school detail model from school list item and sat score data
     */
    private fun getSchoolDetailInfo(
        schoolInfo: SchoolInfo,
        satScore: SchoolSatScore?
    ): SchoolDetailInfo {
        return SchoolDetailInfo(
            schoolInfo.name,
            schoolInfo.location,
            schoolInfo.website,
            phone_number = schoolInfo.phone_number,
            sat_critical_reading_avg_score = satScore?.sat_critical_reading_avg_score ?: "NA",
            sat_math_avg_score = satScore?.sat_math_avg_score ?: "NA",
            sat_writing_avg_score = satScore?.sat_writing_avg_score ?: "NA"
        )
    }
}