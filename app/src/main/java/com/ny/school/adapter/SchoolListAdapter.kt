package com.ny.school.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.ny.school.R
import com.ny.school.ui.SchoolListFragment
import com.ny.school.data.SchoolInfo
import com.ny.school.databinding.SchoolRecyclerViewItemLayoutBinding

class SchoolListAdapter(
    private var schoolInfoList: List<SchoolInfo>,
    var onSchoolSelectedListener: SchoolListFragment.OnSchoolSelectedListener
) : RecyclerView.Adapter<SchoolListAdapter.SchoolItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolItemViewHolder =
        SchoolItemViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.school_recycler_view_item_layout, parent, false
            )
        )

    override fun onBindViewHolder(holder: SchoolItemViewHolder, position: Int) {
        holder.bind(schoolInfoList[position], onSchoolSelectedListener)
    }

    override fun getItemCount(): Int {
        return schoolInfoList.size
    }

    class SchoolItemViewHolder(private val binding: SchoolRecyclerViewItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private var view: View = binding.root

        fun bind(
            schoolInfo: SchoolInfo,
            onSchoolSelectedListener: SchoolListFragment.OnSchoolSelectedListener
        ) {
            binding.school = schoolInfo
            view.setOnClickListener {
                onSchoolSelectedListener.onSchoolSelected(schoolInfo)
            }
        }
    }

}